"use strict";

const url = "https://ajax.test-danit.com/api/swapi/films"
const container = document.querySelector('.container');

fetch(url)
    .then(r => r.json())
    .then(result => {
        result.forEach(
            ({episodeId, characters, openingCrawl, name}) => {

                const filmWrapper = document.createElement('div');
                filmWrapper.classList.add('film-wrapper')
                container.append(filmWrapper)


                filmWrapper.insertAdjacentHTML(
                    "beforeend", `
      <h3 class="film-wrapper_title">Episode ${episodeId} - ${name}</h3>
      <p class="film-wrapper_text">${openingCrawl}</p>`)

                const listCharacters = document.createElement('ul');
                listCharacters.classList.add('film-wrapper__list')
                filmWrapper.append(listCharacters);

                const spinnerLoader = document.createElement('div');
                spinnerLoader.classList.add('spinner');
                spinnerLoader.innerHTML = `<div class="head"></div>`

                listCharacters.append(spinnerLoader);

                characters.forEach(el => {
                    fetch(el)
                        .then(r => r.json())
                        .then(({name}) => {
                            spinnerLoader.remove();
                            listCharacters.insertAdjacentHTML(
                                "beforeend", ` <li class="film-wrapper__item">${name}</li>`)
                        }).catch(err => console.log(err));
                })
            })
    }).catch(err => console.log(err));
