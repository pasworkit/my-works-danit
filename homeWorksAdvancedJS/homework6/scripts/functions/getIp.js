const urlIp = 'https://api.ipify.org/?format=json'

async function getIp() {
    try {
        const {ip} = await fetch(urlIp)
            .then((r) => r.json())
        return ip;
    } catch (error) {
        console.warn(error);
    }
}
export default getIp;