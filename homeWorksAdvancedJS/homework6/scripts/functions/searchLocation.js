import Card from "../classes/Card.js";
import getIp from "./getIp.js";

const container = document.querySelector('.container')

async function searchLocation() {
    try {
        const ip = await getIp();
        const {status, message, continent, country, countryCode, regionName, city, query} = await fetch(
            `http://ip-api.com/json/${ip}?fields=status,message,continent,country,countryCode,regionName,city,query`
        ).then((r) => r.json())
        if (status === "success") {
            new Card(countryCode, query, country, regionName, city, continent).render(container)
        } else {
            alert(message);
        }
    } catch (error) {
        console.warn(error);
    }
}
export default searchLocation;