export default class Card {
    constructor(countryCode, ip, country, regionName, city, continent) {
        this.continent = continent;
        this.countryCode = countryCode;
        this.ip = ip;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
    }

    render(selector) {
        selector.insertAdjacentHTML("beforeend", `<div class="card">
    <ul class="card__list">
        <li class="card__item">${this.countryCode}: ${this.ip}</li>
        <li class="card__item">Континент: ${this.continent}</li>
        <li class="card__item">Країна: ${this.country}</li>
        <li class="card__item">Регіон: ${this.regionName}</li>
        <li class="card__item">Місто: ${this.city}</li>
    </ul>
</div>`)
    }
}