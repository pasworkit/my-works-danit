"use strict"

/*1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Кожен об'єкт в JS має прототип (макет, шаблон), від якого наслідує властивості та методи. Коли ми хочемо прочитати властивість об'єкта а вона відсутня, JS автоматично бере її у прототипа.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
за допомогою super(), викликаємо батьківський конструктор.
*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const kat = new Programmer('Kat', 20, 1000, 'JS');
const alex = new Programmer('Alex', 25, 1500, 'Python');
const max = new Programmer('Max', 30, 2000, 'C#');

console.log(kat);
console.log(alex);
console.log(max);
console.log(kat.salary);