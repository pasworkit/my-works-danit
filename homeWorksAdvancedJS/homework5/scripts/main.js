"use strict";

const urlUsers = "https://ajax.test-danit.com/api/json/users"
const urlPosts = "https://ajax.test-danit.com/api/json/posts"

const container = document.querySelector('.container');
const addButton = document.createElement('button');
addButton.innerHTML = 'Add post';
container.prepend(addButton);

class Card {
    constructor(title, body, userId, id, name, username, email, postId) {
        this.title = title;
        this.body = body;
        this.userId = userId;
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.postId = postId;

        this.cardWrapper = document.createElement('div');
        this.deleteButton = document.createElement('button');
        this.editButton = document.createElement('button');
    }

    createElements() {
        this.cardWrapper.classList.add(`card-wrapper`);
        this.deleteButton.innerHTML = 'Delete';
        this.editButton.innerHTML = 'Edit';
        this.cardWrapper.append(this.deleteButton, this.editButton);

        this.cardWrapper.insertAdjacentHTML(
            "beforeend", `<div>${this.username}---
                <span >${this.email}</span></div>
        <h3 class="card-wrapper__title title-${this.postId}">${this.title}</h3>
        <p class="card-wrapper__text body-${this.postId}">${this.body}</p>`)

        this.deleteButton.addEventListener("click", deleteCard.bind(this));
        this.editButton.addEventListener("click", () => {

            new EditPost(this.id, this.postId).render('.cards__wrapper');
        });
    }

    render(selector) {
        this.createElements();
        document.querySelector(selector).prepend(this.cardWrapper);
    }
}

function deleteCard(e) {
    fetch(`${urlPosts}/${this.userId}`, {
        method: 'DELETE',
    })
        .then(r => {
            if (r.status === 200) {
                e.target.parentElement.remove();
            }
        }).catch(err => {
        console.error(err);
    })
}

function editCard(title, body, id, postId) {
    fetch(`${urlPosts}/${id}`, {
        method: 'PUT',
        body: JSON.stringify({
            title,
            body,
            id
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(r => {
            if (r.status === 200) {
                const newTitle = document.querySelector(`.title-${postId}`);
                newTitle.innerText = title;
                const newBody = document.querySelector(`.body-${postId}`);
                newBody.innerText = body;
            }
        }).catch(err => {
        console.error(err);
    })
}

class Modal {
    constructor() {
        this.modal = document.createElement('div');
        this.modalBackground = document.createElement('div');
        this.modalContent = document.createElement('div');
        this.closeBtnTop = document.createElement('button');
        this.closeBtnTop.innerText='Cancel'

        this.modalForm = document.createElement('form');

        this.modalLabelTitle = document.createElement('label');
        this.modalLabelTitle.innerText = "Title :"
        this.modalInputTitle = document.createElement('input');
        this.modalInputTitle.placeholder = "Enter title"
        this.modalLabelText = document.createElement('label');
        this.modalLabelText.innerText = "Post :"
        this.modalAreaText = document.createElement("textarea");
        this.modalAreaText.placeholder = "Enter text"

        this.modalBtnContainer = document.createElement('div');
        this.closeBtn = document.createElement('button');
        this.closeBtn.innerText = "Close"
        this.submitBtn = document.createElement("button");
        this.submitBtn.className = 'confirm'
        this.submitBtn.innerText = "Confirm"
    }

    closeModal() {
        this.modal.remove();
    }

    createElements() {
        this.modal.classList.add('modal');
        this.modalBackground.classList.add('modal__background');
        this.modalContent.classList.add('modal__content-wrapper');
        this.modalForm.classList.add("modal__form");
        this.modalBtnContainer.classList.add('modal__button-wrapper');

        this.modal.append(this.modalBackground);
        this.modalBackground.append(this.closeBtnTop, this.modalContent)
        this.modalBtnContainer.append(this.closeBtn, this.submitBtn)
        this.modalContent.append(this.modalForm)
        this.modalForm.append(this.modalLabelTitle, this.modalInputTitle, this.modalLabelText, this.modalAreaText, this.modalBtnContainer);


        this.modalForm.addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                event.preventDefault();
                document.querySelector(".confirm").click();
            }
        });
        this.closeBtnTop.addEventListener('click', this.closeModal.bind(this));
        this.closeBtn.addEventListener('click', this.closeModal.bind(this));
        this.submitBtn.addEventListener('click', (e) => {
            e.preventDefault();
            this.closeModal();
        });
    }

    render() {
        this.createElements();
        document.querySelector('.cards__wrapper').append(this.modal);
    }
}

addButton.addEventListener('click', () => {
    new CreatePost().render();
})

class CreatePost extends Modal {
    constructor() {
        super();
    }

    createElements() {
        super.createElements();
        this.submitBtn.addEventListener("click", () => {
            createNewPost(this.modalInputTitle.value, this.modalAreaText.value);
            this.closeModal();
        });
    }
}

function createNewPost(title, body) {
    fetch(urlPosts, {
        method: "POST",
        body: JSON.stringify({
            userId: 1,
            title,
            body,
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(r => r.json())
        .then(({title, body, userId, id: postId}) => {

            fetch("https://ajax.test-danit.com/api/json/users/1")
                .then((r) => r.json())
                .then(({id, name, username, email}) => {

                    new Card(title, body, userId, id, name, username, email, postId).render('.cards__wrapper');
                })
            //     showCards();  // if server save info
        }).catch(err => {
        console.error(err);
    });
}

class EditPost extends Modal {
    constructor(id, postId) {
        super();
        this.id = id;
        this.postId = postId;
    }

    createElements() {
        super.createElements();
        this.modalInputTitle.value = document.querySelector(`.title-${this.postId}`).textContent;
        this.modalAreaText.value = document.querySelector(`.body-${this.postId}`).textContent;

        this.submitBtn.addEventListener('click', () => {
            editCard(this.modalInputTitle.value, this.modalAreaText.value, this.id, this.postId)
        })
    }
}

const
    showCards = () => {
        fetch(urlPosts)
            .then(r => r.json())
            .then(posts => {
                fetch(urlUsers)
                    .then(r => r.json())
                    .then(users => {
                        posts.forEach(({title, body, userId, id: postId}) => {
                            users.forEach(({id, name, username, email}) => {
                                if (userId === id) {
                                    new Card(title, body, userId, id, name, username, email, postId).render('.cards__wrapper');
                                }
                            })
                        });
                    })
            }).catch(err => {
            console.error(err);
        })
    }

showCards();



