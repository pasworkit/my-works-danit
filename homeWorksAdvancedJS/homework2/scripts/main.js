'use strict'

/*Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
* - ми не впевненні в інформації з бекенду,
* - коли введена інфа від юзера може безпосередньо вплинути на наш код,
* - робота з сторонніми API i т.д
*
* */

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const container = document.querySelector('#root');
const list = document.createElement("ul");
container.append(list);

class NotValue extends Error {
    constructor(value) {
        super();
        this.name = "NotValue";
        this.message = `Not Value ${value} in Book `;
    }
}


class Book {
    constructor(author, name, price) {
        if (author === undefined) {
            throw new NotValue('author')
        } else if (name === undefined) {
            throw new NotValue('name')
        } else if (price === undefined) {
            throw new NotValue('price')
        }

        this.author = author;
        this.name = name;
        this.price = price;
    }

    render(list) {
        list.insertAdjacentHTML("beforeend", `<li>
<div>
<p>${this.author}</p>
<p>${this.name}</p>
<p>${this.price}</p>
</div>
</li>`)
    }
}


books.forEach((book) => {
    try {
        new Book(book.author, book.name, book.price).render(list)
    } catch (err) {
        if (err.name === "NotValue") {
            console.warn(err);
        } else {
            throw err;
        }
    }
})




