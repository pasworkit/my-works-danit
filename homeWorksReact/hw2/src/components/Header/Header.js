import {PureComponent} from 'react';
import styles from './Header.module.scss';
import cartSvg from '../../svg/cart-outline.svg';
import heartSvg from '../../svg/heart.svg';
import heartSvgOutline from '../../svg/heart-outline.svg';
import PropTypes from "prop-types";


class Header extends PureComponent {

    render() {
        const {productCount, favoritesCount} = this.props;

        return (
            <header className={styles.root}>

                <nav>
                    <ul>
                        <li>Книги</li>
                    </ul>
                </nav>

                {/* Right container */}
                <ul>
                    <li><img src={cartSvg} alt='cart'/>{productCount}</li>
                    <li>

                        {favoritesCount === 0 ? (
                            <img src={heartSvgOutline} alt="heart"/>
                        ) : (
                            <img src={heartSvg} alt="heart"/>
                        )} {favoritesCount}
                    </li>

                </ul>
            </header>
        );
    }
}

Header.propTypes = {
    productCount: PropTypes.number,
    favoritesCount: PropTypes.number,
};


export default Header;
