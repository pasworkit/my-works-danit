import React, {PureComponent} from 'react';
import CardItem from '../CardItem/CardItem';
import styles from './CardContainer.module.scss';
import PropTypes from "prop-types";

class CardContainer extends PureComponent {
    render() {
        const {cards, addToCart, addToFavorite} = this.props;
        return (
            <div>
                <ul className={styles.list}>
                    {cards.map(({title, img, price, id, isFavorite}) => (
                        <li key={id}>
                            <CardItem
                                addToCart={addToCart}
                                id={id}
                                title={title}
                                price={price}
                                img={img}
                                addToFavorite={addToFavorite}
                                isFavorite={isFavorite}
                            />
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

CardContainer.propTypes = {
    cards: PropTypes.array,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func,
};

CardContainer.defaultProps = {
    cards: []
};


export default CardContainer;
