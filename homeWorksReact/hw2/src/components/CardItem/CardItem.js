import {PureComponent} from 'react';
import styles from './CardItem.module.scss';
import Button from '../Button/Button';
import outlineHeartSvg from '../../svg/heart-outline.svg'
import heartSvg from '../../svg/heart.svg'
import PropTypes from "prop-types";

class CardItem extends PureComponent {
    state = {
        favoriteState: false
    }

    updateFavorite() {
        this.setState((current) => {
            return {favoriteState: (!current.favoriteState)}
        })
    }


    render() {
        const {addToCart, title, img, price, id, addToFavorite} = this.props;

        return (
            <div className={styles.card}>
                <button onClick={() => {
                    addToFavorite(id);
                    this.updateFavorite();
                }} type="button" className={styles.likeButton}>
                    {this.state.favoriteState ? (
                        <img src={heartSvg} alt="heart"/>
                    ) : (
                        <img src={outlineHeartSvg} alt="heart"/>
                    )}
                </button>
                <div className={styles.article}>Артикул: {id}</div>
                <div className={styles.title}>{title}</div>
                <div>{price} грн</div>
                <img className={styles.itemAvatar} src={img}
                     alt={title}/>


                <div className={styles.btnContainer}>
                    <Button onClick={() =>
                        addToCart({title, img, id})
                    }>До кошика</Button>
                </div>
            </div>
        )
    }
}

CardItem.propTypes = {
    addToCart: PropTypes.func,
    id: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    img: PropTypes.string,
    addToFavorite: PropTypes.func,
    isFavorite: PropTypes.bool,
}

export default CardItem;