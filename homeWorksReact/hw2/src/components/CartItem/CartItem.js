import {PureComponent} from 'react';
import styles from './CartItem.module.scss';
import Button from '../Button/Button'
import PropTypes from "prop-types";

class CartItem extends PureComponent {

    render() {
        const {title, img, count, id, incrementCartItem, decrementCartItem, toggleModal, setModalProps} = this.props
        return (
            <div className={styles.cartItem}>
                <div className={styles.contentContainer}>
                    <div className={styles.imgWrapper}>
                        <img className={styles.itemAvatar} src={img}
                             alt={title}/>
                    </div>
                </div>

                <span className={styles.quantity}>{count}</span>

                <div className={styles.btnContainer}>
                    <Button onClick={() => incrementCartItem(id)} className={styles.btn}>+</Button>
                    <Button onClick={() => decrementCartItem(id)} className={styles.btn}>-</Button>
                    <Button onClick={() => {
                        setModalProps({id, title});
                        toggleModal(true);
                    }} color="red" className={styles.btn}>DEL</Button>
                </div>

            </div>
        )
    }
}

CartItem.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
    count: PropTypes.number,
    id: PropTypes.string,
    incrementCartItem: PropTypes.func,
    decrementCartItem: PropTypes.func,
    toggleModal: PropTypes.func,
    setModalProps: PropTypes.func,
}

export default CartItem;