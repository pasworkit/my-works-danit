import {PureComponent} from 'react';
import CartItem from '../CartItem/CartItem'

class CartContainer extends PureComponent {

    render() {

        const {carts, incrementCartItem, decrementCartItem, toggleModal, setModalProps} = this.props

        return (
            <ul>
                {carts.map(({title, img, count, id}) => (
                    <li key={id}>

                        <CartItem setModalProps={setModalProps} toggleModal={toggleModal} title={title} img={img}
                                  count={count} id={id} incrementCartItem={incrementCartItem}
                                  decrementCartItem={decrementCartItem}/>
                    </li>
                ))}
            </ul>
        )
    }

}

export default CartContainer;