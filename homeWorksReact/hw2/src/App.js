import React, {Component} from 'react';
import styles from './App.module.scss';
import Header from './components/Header/Header';
import Preloader from './components/Preloader/Preloader';
import Modal from './components/Modal/Modal';
import CardContainer from "./components/CardContainer/CardContainer";
import CartContainer from "./components/CartContainer/CartContainer";

class App extends Component {
    state = {
        cards: [],
        carts: [],
        favorites: [],
        isCardsLoading: true,
        isOpenModal: false,
        modalProps: {},
        productCount: 0,
        favoritesCount: 0
    }

    addToCart = (card) => {
        this.setState((current) => {
            current.productCount += 1;
            const carts = [...current.carts]
            const index = carts.findIndex(el => el.id === card.id)

            if (index === -1) {
                carts.push({...card, count: 1})
            } else {
                carts[index].count += 1
            }
            localStorage.setItem("carts", JSON.stringify(carts))
            localStorage.setItem('productCount', JSON.stringify(this.state.productCount));
            return {carts}
        })
    }

    incrementCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]
            current.productCount += 1;

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1) {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            localStorage.setItem('productCount', JSON.stringify(this.state.productCount));
            return {carts}
        })
    }

    decrementCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1 && carts[index].count > 1) {
                carts[index].count -= 1
                current.productCount -= 1;
                localStorage.setItem('productCount', JSON.stringify(this.state.productCount));
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return {carts}
        })
    }

    deleteCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1) {
                let cart = carts[index];
                current.productCount -= cart.count;
                carts.splice(index, 1);
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            localStorage.setItem('productCount', JSON.stringify(this.state.productCount));

            return {carts}
        })
    }

    toggleModal = (value) => {
        this.setState({isOpenModal: value})
    }

    setModalProps = (value) => {
        this.setState({modalProps: value})
    }


    addToFavorite = (cardId) => {
        this.setState((current) => {
            let foundCards = current.cards.filter(card => card.id === cardId);
            let card = foundCards[0]
            let previousFavorite = card.isFavorite;
            card.isFavorite = !previousFavorite;

            let favouriteCards = current.cards.filter(({isFavorite}) => isFavorite);

            current.favoritesCount = favouriteCards.length;
            const favorites = [...current.favorites]
            const index = favorites.findIndex(el => el.id === cardId)


            if (index === -1) {
                favorites.push({...card, count: 1})
            } else {
                favorites[index].count += 1
            }
            localStorage.setItem("favorites", JSON.stringify(favorites))
            localStorage.setItem('favoritesCount', JSON.stringify(this.state.favoritesCount));
            return current;
        })
    }


    async componentDidMount() {
        const cards = await fetch('./products.json').then(res => res.json());
        const carts = localStorage.getItem('carts');
        const productCount = localStorage.getItem('productCount');
        const favorites = localStorage.getItem('favorites');
        const favoritesCount = localStorage.getItem('favoritesCount');

        if (carts) {
            this.setState({carts: JSON.parse(carts)})
            this.setState({productCount: JSON.parse(productCount)})
            this.setState({favorites: JSON.parse(favorites)})
            this.setState({favoritesCount: JSON.parse(favoritesCount)})
        }
        this.setState({cards, isCardsLoading: false});
    }


    render() {
        const {
            isCardsLoading,
            cards,
            carts,
            favorites,
            isOpenModal,
            modalProps,
            productCount,
            favoritesCount
        } = this.state;

        return (
            <div className={styles.App}>
                <Header productCount={productCount} favoritesCount={favoritesCount}/>
                <main>
                    <section className={styles.leftContainer}>
                        <h1>Каталог</h1>
                        {isCardsLoading && <Preloader size={90}/>}
                        {!isCardsLoading &&
                            <CardContainer addToCart={this.addToCart} cards={cards} addToFavorite={this.addToFavorite}
                            />}
                    </section>

                    <section className={styles.rightContainer}>
                        <h2>Корзина</h2>
                        <CartContainer
                            setModalProps={this.setModalProps}
                            toggleModal={this.toggleModal}
                            incrementCartItem={this.incrementCartItem}
                            decrementCartItem={this.decrementCartItem}
                            carts={carts}
                            cards={cards}
                            favorites={favorites}
                        />
                    </section>
                </main>
                <Modal modalProps={modalProps} deleteCartItem={this.deleteCartItem} isOpen={isOpenModal}
                       toggleModal={this.toggleModal}/>
            </div>
        );
    }
}

export default App;
