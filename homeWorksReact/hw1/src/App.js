import React, {Component} from 'react';
import styles from './App.module.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends Component {
    state = {
        isModal: false,
    }

    openModal = (modalToOpen) => this.setState({isModal: modalToOpen});
    closeModal = () => this.setState({isModal: false});

    render() {
        const {isModal} = this.state;
        return (
            <div className={styles.container}>
                <div className={styles.buttonContainer}>
                    <Button text="Open first modal" backgroundColor="blue" onClick={() => this.openModal('1')}/>
                    <Button text="Open second modal" backgroundColor="red" onClick={() => this.openModal('2')}/>
                </div>

                {isModal === '1' && <Modal
                    header='Do you want to delete this file?'
                    text='Once you delete this file, it won’t be possible to undo this action.
				Are you sure you want to delete it?'
                    handleOnClose={this.closeModal}
                    closeButton
                    actions={
                        <div>
                            <Button text='Ok' backgroundColor="rgb(0, 0, 0, 0.3)" onClick={this.closeModal}/>
                            <Button text='Cancel' backgroundColor="rgb(0, 0, 0, 0.3)" onClick={this.closeModal}/>
                        </div>
                    }
                />}
                {isModal === '2' && <Modal
                    header='Modal number second'
                    text='Hello, I am modal window number 2!'
                    handleOnClose={this.closeModal}
                    closeButton
                    actions={
                        <div>
                            <Button text='Console-1' backgroundColor="green" onClick={() => {
                                console.log('11111111')
                                this.closeModal(1)
                            }}

                            />
                            <Button text='Console-2' backgroundColor="blue" onClick={() => {
                                console.log('2222222')
                                this.closeModal(1)
                            }}

                            />
                        </div>
                    }
                />}
            </div>
        );
    }
}

export default App;
