import React, {PureComponent} from "react";
import styles from "./Button.module.scss";

class Button extends PureComponent {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        return (
            <button
                className={styles.button}
                type="button"
                style={{backgroundColor}}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

export default Button;
