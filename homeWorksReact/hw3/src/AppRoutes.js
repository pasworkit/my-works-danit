import {Route, Routes} from 'react-router-dom';
import FavoritePage from './pages/FavoritePage/FavoritesPage';
import HomePage from './pages/HomePage/HomePage';
import CartPage from "./pages/CartPage/CartPage";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";
import {useEffect, useState} from "react";
import Header from "./components/Header/Header";

const AppRoutes = () => {

    const [favorites, setFavorites] = useState([]);
    const [favoritesCount, setFavoritesCount] = useState(0);
    const [carts, setCarts] = useState([]);
    const [cartsCount, setCartsCount] = useState(0);
    const [cards, setCards] = useState([]);
    const [modalProps, setModalProps] = useState({});
    const [isOpenModal, setIsOpenModal] = useState(false);


    useEffect(() => {
        const cards = localStorage.getItem("cards");
        const cartsCount = localStorage.getItem("cartsCount");
        const favoritesCount = localStorage.getItem("favoritesCount");
        const favorites = localStorage.getItem("favorites");
        const carts = localStorage.getItem("carts");

        if (cards) {
            setCards(JSON.parse(cards));
        } else {
            (async () => {
                const cards = await fetch("./products.json").then((res) => res.json());
                localStorage.setItem("cards", JSON.stringify(cards));
                setCards(cards);
            })();
        }

        if (cartsCount) {
            setCartsCount(JSON.parse(cartsCount));
        }

        if (favoritesCount) {
            setFavoritesCount(JSON.parse(favoritesCount));
        }

        if (favorites) {
            setFavorites(JSON.parse(favorites));
        }

        if (carts) {
            setCarts(JSON.parse(carts));
        }
    }, []);


    const addToCart = (card) => {
        setCarts((prevCarts) => {
            const index = prevCarts.findIndex(el => el.id === card.id)

            if (index === -1) {
                prevCarts.push({...card, count: 1})
            } else {
                prevCarts[index].count += 1
            }

            setCartsCount(prevCardsCount => prevCardsCount + 1)
            return prevCarts
        });
    }

    const addToFavorite = (id) => {
        setCards(prevCards => {
            const index = prevCards.findIndex(el => el.id === id)
            prevCards[index].isFavorite = !prevCards[index].isFavorite
            return [...prevCards];
        })
        setFavorites((prevFavorites) => {
            const index = prevFavorites.findIndex(el => el.id === id)
            const cardIndex = cards.findIndex(el => el.id === id)

            if (index === -1) {
                prevFavorites.push({...cards[cardIndex], isFavorite: true})
            } else {
                prevFavorites.splice(index, 1)
            }

            setFavoritesCount(prevFavorites.length)
            return [...prevFavorites]
        })
        return {favorites, cards}
    };


    const incrementCartItem = (id) => {
        setCarts((current) => {
            const cart = [...current]
            const index = cart.findIndex(el => el.id === id)

            if (index !== -1) {
                current[index].count += 1
            }
            setCartsCount(prevCardsCount => prevCardsCount + 1)
            return cart
        })
    }

    const decrementCartItem = (id) => {
        setCarts((current) => {
            const cart = [...current]
            const index = cart.findIndex(el => el.id === id)

            if (index !== -1 && current[index].count > 1) {
                current[index].count -= 1
                setCartsCount(prevCardsCount => prevCardsCount - 1)
            }
            return cart
        })
    }

    const deleteCartItem = (id) => {
        setCarts((current) => {
            const cart = [...current]
            const index = cart.findIndex(el => el.id === id)

            if (index !== -1) {
                cart.splice(index, 1);
                setCartsCount(prevCardsCount => prevCardsCount - current[index].count)
            }
            return cart
        })
    }

    const toggleModal = (value) => {
        setIsOpenModal(value)
    }

    useEffect(() => {
        localStorage.setItem('cards', JSON.stringify(cards));
        localStorage.setItem('carts', JSON.stringify(carts));
        localStorage.setItem('favorites', JSON.stringify(favorites));
        localStorage.setItem('cartsCount', JSON.stringify(cartsCount));
        localStorage.setItem('favoritesCount', JSON.stringify(favoritesCount));
    }, [cards, carts, favorites, cartsCount, favoritesCount]);


    return (
        <>
            <Header favoritesSize={favoritesCount} cartsSize={cartsCount}/>
            <Routes>
                <Route path='/' element={
                    <HomePage
                        addToCart={addToCart}
                        addToFavorite={addToFavorite}
                        cards={cards}
                    />}></Route>
                <Route path='/favorites' element={
                    <FavoritePage
                        favorites={favorites}
                        addToCart={addToCart}
                        addToFavorite={addToFavorite}
                    />}></Route>
                <Route path='/cart' element={
                    <CartPage carts={carts}
                              incrementCartItem={incrementCartItem}
                              setModalProps={setModalProps}
                              toggleModal={toggleModal}
                              decrementCartItem={decrementCartItem}
                              deleteCartItem={deleteCartItem}
                              modalProps={modalProps}
                              isOpenModal={isOpenModal}

                    />}></Route>
                <Route path='*' element={<NotFoundPage/>}></Route>
            </Routes>
        </>
    )
}

export default AppRoutes;