import CartContainer from "../../components/CartContainer/CartContainer";
import Modal from "../../components/Modal/Modal";
import styles from "../CartPage/CartPage.module.scss";

const CartPage = ( {carts, incrementCartItem, setModalProps, toggleModal, decrementCartItem, deleteCartItem, modalProps, isOpenModal}) => {

    return (
        <div>
            <section className={styles.wrapper}>
                <h1>Koшик</h1>
            <CartContainer
                setModalProps={setModalProps}
                toggleModal={toggleModal}
                incrementCartItem={incrementCartItem}
                decrementCartItem={decrementCartItem}
                carts={carts}
            />
            <Modal modalProps={modalProps} deleteCartItem={deleteCartItem} isOpen={isOpenModal}
                   toggleModal={toggleModal}/>
            </section>
        </div>
    )
}
export default CartPage;