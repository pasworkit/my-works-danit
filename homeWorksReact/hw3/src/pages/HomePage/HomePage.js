import styles from "../HomePage/HomePage.module.scss";
import CardContainer from "../../components/CardContainer/CardContainer";

const HomePage = ({addToFavorite, cards, addToCart}) => {
    return (
        <div>
            <section className={styles.wrapper}>
                <h1>Каталог</h1>
                <CardContainer addToCart={addToCart} cards={cards} addToFavorite={addToFavorite}
                />
            </section>
        </div>
    )
}
export default HomePage;