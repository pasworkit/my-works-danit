import FavoriteContainer from "../../components/FavoriteConteiner/FavoriteConteiner";
import styles from '../FavoritePage/FavoritePage.module.scss'

const FavoritesPage = ({favorites,addToCart, addToFavorite }) => {

    return (
        <div>
            <section className={styles.wrapper}>
            <h1>Топ добірка:</h1>
            <FavoriteContainer favorites={favorites}
                               addToCart={addToCart}
                               addToFavorite={addToFavorite}/>
                </section>
        </div>
    )
}
export default FavoritesPage;