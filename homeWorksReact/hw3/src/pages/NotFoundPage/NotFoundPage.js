import styles from '../NotFoundPage/NotFoundPage.module.scss'

const NotFoundPage = () => {
    return (
        <div>
            <h2 className={styles.title}> This page is not found </h2>
        </div>
    )
}

export default NotFoundPage