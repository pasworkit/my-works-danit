import CartItem from '../CartItem/CartItem'
import styles from '../CardContainer/CardContainer.module.scss'

const CartContainer = ({carts, incrementCartItem, decrementCartItem, toggleModal, deleteCartItem, setModalProps}) => {
    return (
        <div>
            <ul className={styles.list}>
                {carts.map(({title, img, price, count, id}) => (
                    <li key={id}>
                        <CartItem toggleModal={toggleModal}
                                  title={title}
                                  img={img}
                                  count={count}
                                  price={price}
                                  id={id}
                                  incrementCartItem={incrementCartItem}
                                  decrementCartItem={decrementCartItem}
                                  deleteCartItem={deleteCartItem}
                                  setModalProps={setModalProps}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default CartContainer;