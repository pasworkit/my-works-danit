import styles from './CartItem.module.scss';
import Button from '../Button/Button'
import PropTypes from "prop-types";
import React from "react";

const CartItem = ({title, img, count, id, price, incrementCartItem, decrementCartItem, toggleModal, setModalProps}) => {
    return (
        <div className={styles.card}>
            <div className={styles.btnContainer}>
                <Button onClick={() => incrementCartItem(id)} className={styles.btn}>+</Button>
                <Button onClick={() => decrementCartItem(id)} className={styles.btn}>-</Button>
            </div>
            <div className={styles.article}>Артикул: {id}</div>
            <div className={styles.title}>{title}</div>
            <div>Ціна: {price} грн</div>
            <img className={styles.itemAvatar} src={img}
                 alt={title}/>

            <div className={styles.quantity}>Кількість: {count}</div>
            <div className={styles.btnContainerTop}>
                <Button onClick={() => {
                    setModalProps({id, title});
                    toggleModal(true);
                }} color="red" className={styles.likeButton}>X</Button>
            </div>

        </div>
    )
}

CartItem.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
    count: PropTypes.number,
    id: PropTypes.string,
    incrementCartItem: PropTypes.func,
    decrementCartItem: PropTypes.func,
    toggleModal: PropTypes.func,
    setModalProps: PropTypes.func,
}

export default CartItem;