import styles from './Header.module.scss';
import cartSvg from '../../svg/cart-outline.svg';
import heartSvg from '../../svg/heart.svg';
import heartSvgOutline from '../../svg/heart-outline.svg';
import PropTypes from "prop-types";
import Navigation from "../Navigation/Navigation";
import {useNavigate} from "react-router-dom";

const Header = ({favoritesSize, cartsSize}) => {

    const navigate = useNavigate();
    return (
        <header className={styles.root}>
            <Navigation/>
            <ul>
                <li><button className={styles.btn} onClick={()=>{navigate('/cart')}}>
                    <img src={cartSvg} alt='cart'/>{cartsSize}</button>
                    </li>

                <li><button className={styles.btn} onClick={()=>{navigate('/favorites')}}>{favoritesSize === 0 ? (
                    <img src={heartSvgOutline} alt="heart"/>
                ) : (
                    <img src={heartSvg} alt="heart"/>
                )} {favoritesSize}
                    </button>
                </li>
            </ul>
        </header>
    );
}

Header.propTypes = {
    productCount: PropTypes.number,
    favoritesCount: PropTypes.number,
};

export default Header;
