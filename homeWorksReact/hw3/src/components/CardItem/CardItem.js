import styles from './CardItem.module.scss';
import Button from '../Button/Button';
import React, {useState} from 'react'
import outlineHeartSvg from '../../svg/heart-outline.svg'
import heartSvg from '../../svg/heart.svg'
import PropTypes from "prop-types";

const CardItem = ({addToCart, card, addToFavorite}) => {

    const [favoriteState, setFavoriteState] = useState(card.isFavorite);

    return (
        <div className={styles.card}>
            <button onClick={() => {
                setFavoriteState(favoriteState => !favoriteState);
                addToFavorite(card.id);
            }} type="button" className={styles.likeButton}>
                {favoriteState ? (
                    <img src={heartSvg} alt="heart"/>
                ) : (
                    <img src={outlineHeartSvg} alt="heart"/>
                )}
            </button>
            <div className={styles.article}>Артикул: {card.id}</div>
            <div className={styles.title}>{card.title}</div>
            <div>{card.price} грн</div>
            <img className={styles.itemAvatar} src={card.img}
                 alt={card.title}/>


            <div className={styles.btnContainer}>
                <Button onClick={() =>
                    addToCart(card)
                }>До кошика</Button>
            </div>
        </div>
    )
}

CardItem.propTypes = {
    addToCart: PropTypes.func,
    id: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    img: PropTypes.string,
    addToFavorite: PropTypes.func,
    isFavorite: PropTypes.bool,
}


export default CardItem;