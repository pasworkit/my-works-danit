import CardItem from '../CardItem/CardItem';
import styles from './CardContainer.module.scss';
//import PropTypes from "prop-types";

const CardContainer = ({cards, addToCart, addToFavorite}) => {
    return (
        <div>
            <ul className={styles.list}>
                {cards.map((card) => (
                    <li key={card.id}>
                        <CardItem
                            addToCart={addToCart}
                            card={card}
                            addToFavorite={addToFavorite}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
}

// CardContainer.propTypes = {
//     cards: PropTypes.array,
//     addToCart: PropTypes.func,
//     addToFavorite: PropTypes.func,
// };
//
// CardContainer.defaultProps = {
//     cards: []
// };

export default CardContainer;
