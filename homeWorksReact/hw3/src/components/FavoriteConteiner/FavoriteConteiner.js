import CardItem from "../CardItem/CardItem";
import styles from "../FavoriteConteiner/FavoriteContainer.module.scss"

const FavoriteContainer = ({favorites, addToFavorite, addToCart}) => {
    return (
        <div>
            <ul className={styles.list}>
                {favorites.map(card => (
                    <li key={card.id}>
                        <CardItem addToFavorite={addToFavorite}
                                  addToCart={addToCart}
                                  card={card}/>
                    </li>
                ))}
            </ul>
        </div>
    );
}


export default FavoriteContainer;
