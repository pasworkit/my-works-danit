import styles from './Button.module.scss';
import React, {useState} from 'react'

const colorsMap = {
    blue: styles.blue,
    red: styles.red,
}

const Button = ({children, color, onClick, className, type}) => {


    const [isPressed, setIsPressed] = useState(false);
    const pressedStyle = isPressed ? styles.pressed : '';

    return (
        <button
            onMouseDown={() => setIsPressed((isPressed) => !isPressed)}
            onMouseUp={() => setIsPressed((isPressed) => !isPressed)}
            onClick={onClick}
            className={`${styles.btn} ${pressedStyle} ${colorsMap[color]} ${className}`}
            type={type}
        >
            {children}
        </button>
    )
}

export default Button;