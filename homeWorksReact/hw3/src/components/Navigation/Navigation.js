import {NavLink} from 'react-router-dom'

const Navigation = () => {

    return (
        <nav>
            <ul>
                <li>
                    <NavLink to="/">Home Page</NavLink>
                </li>
                <li>
                    <NavLink to="/favorites">Favorites</NavLink>
                </li>
                <li>
                    <NavLink to="/cart">Cart</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navigation