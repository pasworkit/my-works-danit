/*
1. Опишіть своїми словами що таке Document Object Model (DOM);
Це реалізація документа - програмно, у вигляді дерева - структури з вузлів та обєктів, які мають свої властивості та методи.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerText - працює з текстом елементів, а innerHTML - окрім тексту, дає змогу працювати з тегами та стилями.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

document.querySelectorAll(css) - найбільш універсальний!
document.querySelector(css)
document.getElementById(id)
document.getElementsByName(name)
document.getElementsByClassName(className)
document.getElementsByTagName(tag)
*/


// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000;

const paragraphCollection = document.querySelectorAll("p");
paragraphCollection.forEach(item => {
    item.style.background = '#ff0000';
});

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const elemOptionsList = document.querySelector('#optionsList');
console.log(elemOptionsList);
const ancestorOptionsList = elemOptionsList.parentElement;
console.log(ancestorOptionsList);
const childrenOptionsList = elemOptionsList.children;
console.log(childrenOptionsList);

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

const textParagraph = document.querySelector('#testParagraph');
textParagraph.innerHTML = 'This is a paragraph';

// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const liCollection = document.querySelectorAll(".main-header li");
console.log(liCollection);
for (let li of liCollection) {
    li.className = 'nav-item';
}

//5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const elemCollection = document.querySelectorAll(".section-title");
elemCollection.forEach(item => {
    item.classList.remove('section-title')
});