/*
1. Опишіть своїми словами як працює метод forEach.
forEach виконує функцію коллбек, один раз для кожного елементу;

2. Як очистити масив?
arr.length = 0;
arr = [];
arr.splice(0, arr.length);

3. Як можна перевірити, що та чи інша змінна є масивом?
За допомогою методу Array.isArray() повертає true, це масив, і відповідно false, якщо ні.
 */

let arr = ['hello', 'world', 23, '23', null];

const filterBy = (arr, type) => arr.filter(elem => {
    if (type === "null" && elem === null) {
        return false;
    } else if (type === "object" && elem === null) {
        return true;
    }
    return typeof elem !== type;

})

filterBy(arr, "string");


const newFilterBy2 = [];
const filterBy2 = (arr, type) => arr.forEach((elem) => {
    if (typeof elem !== type && type !== "null" || type === "null" && elem !== null || type === "object" && elem === null) {
        newFilterBy2.push(elem);
    }
    return newFilterBy2;
})

filterBy2(arr, "string");
