/*
1. Описать своими словами для чего вообще нужны функции в программировании.
Функціі потрібні для того, аби менше дублювати код, достатньо просто викликати потрібну функцію в потрібному місці.
 Плюс функціі допомагають структурувати код.

2. Описать своими словами, зачем в функцию передавать аргумент.
Агргументи це є безпосередньо дані, з якими працює фунція.

3. Что такое оператор return и как он работает внутри функции?
Оператор return закінчує виконнаня функції, виконання переходить до виклику функції також може передавати значення.

 */

// function validateIsNumber(userNumber) {
//     return !(isNaN(+userNumber) || userNumber === null || userNumber === "");
// }
//
// function getUserNumber(massage = 'enter number') {
//     let userNum = null;
//     do {
//         userNum = prompt(massage, [userNum])
//     } while (!validateIsNumber(userNum))
//
//     return +userNum;
// }
// function getUserOperator(massage = 'enter operator') {
//     let userOper = null;
//     do {
//         userOper = prompt(massage, [userOper])
//     } while (!(userOper === "-" || userOper === "+" || userOper === "*" || userOper === "/"))
//
//     return userOper;
// }
//
// function calcResult() {
//     let firstNumber = getUserNumber('Enter first number');
//     let secondNumber = getUserNumber('Enter second number');
//     let operator = getUserOperator("Enter the operator to calculate (-, +, *, /).");
//
//     switch (operator) {
//         case '-':
//             return firstNumber - secondNumber;
//         case '+':
//             return firstNumber + secondNumber;
//         case '*':
//             return firstNumber * secondNumber;
//         case '/':
//             return firstNumber / secondNumber;
//         default:
//             alert("There is no such operator");
//     }
// }
// console.log(calcResult());

const numberValidator = (value) => isNaN(+value) || value === null || value === "";
const operatorValidator = (value) => !(value === "-" || value === "+" || value === "*" || value === "/");

function getValue(message, validator) {

    let inputValue = null;

    do {
        inputValue = prompt(message, [inputValue])
    } while (validator(inputValue))

    return inputValue;
}

function calcResult() {

    let firstNumber = +getValue('Enter first number', numberValidator);
    let secondNumber = +getValue('Enter second number', numberValidator);
    let operator = getValue("Enter the operator to calculate (-, +, *, /).", operatorValidator);

    switch (operator) {
        case '-':
            return firstNumber - secondNumber;
        case '+':
            return firstNumber + secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
        default:
            alert("There is no such operator");
    }
}

console.log(calcResult());