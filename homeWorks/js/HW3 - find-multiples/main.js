"use strict"
/*
1. Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
Сама назава цикл "яскраво" говорить про своє призначення.
Цикли в програмуванні потрібні для того, аби багаторазово (потрібну кількість) повторювати однотипні дії (однотипний код).
2. Опишите в каких ситуациях вы бы использовали тот или иной цикл в JS.
do..while - для валідації данних від юзера;
for - для пошуку потрібного значення або сортування;
while - для організації "вічного" циклу.
3. Что такое явное и неявное приведение (преобразование) типов данных в JS?
Неявне приведення, це коли компілятор автоматично назначає тип данних;
Явне приведення, це коли девелопер за допомогую коду виражає намір конвертації;
 */


let userRandomNumber = prompt("Enter your random number, please.");
while (userRandomNumber === "" || userRandomNumber === null || isNaN(+userRandomNumber) ||
!(Number.isInteger(+userRandomNumber))) {
    userRandomNumber = prompt("Enter your random number, please.", [userRandomNumber]);
}


for (let i = 0; i <= userRandomNumber; i++) {
    if (userRandomNumber < 5) {
        console.log("`Sorry, no numbers'")
        break;
    } else if (i % 5 === 0) {
        console.log(i);
    }
}

console.log( "Додаткове завдання:" );

let m = 0;
let n = 0;
do {
    m = prompt("Enter your random number m > 1, please.");
    while (m === "" || m === null || isNaN(+m) || m <= 1) {
        m = prompt("Enter your random number m > 1, please.");
    }
    n = prompt("Enter your random number n, please.");
    while (n === "" || n === null || isNaN(+n)) {
        n = prompt("Enter your random number n, please.");
    }
    if ( m >= n) {
        alert("Error, the number m must be less than n and not be equal ");
    }
} while (m >= n)


nextPrime: for (let i = m; i < n; i++) {
    for (let j = m; j < i; j++) {
        if (i % j === 0) continue nextPrime;
    }

    console.log( i );
}

