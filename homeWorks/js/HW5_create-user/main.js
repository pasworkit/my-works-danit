/*
1. Опишіть своїми словами, що таке метод об'єкту.
Функції, які знаходяться в об'єкті (властивостях обєкту) це - методи. Методи дають можливисть щось роби обєкту,
тобто діяти.
2. Який тип даних може мати значення властивості об'єкта?
Значенням властивості об'єкта може бути будь-який тип данних.
3. Об'єкт це посилальний тип даних. Що означає це поняття?
Змінна яка має - посилальний тип даних, фактично не містить значення в особі, а посилається на "ячейку" памяті в якій зберігаються данні.
 */


const valueValidator = (value) => value === null || value === "";

function getValue(message, validator) {

    let inputValue = null;

    do {
        inputValue = prompt(message)
    } while (validator(inputValue))

    return inputValue;
}

function createNewUser() {
    return {
        firstName: getValue('Enter first name', valueValidator),
        lastName: getValue('Enter last name', valueValidator),

        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    }
}

const myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());


Object.defineProperties(myUser, {
    firstName: {
        writable: false,
        configurable: true
    },
    lastName: {
        writable: false,
        configurable: true,
    }
});

myUser.setNewFirstName = function (newFName) {
    Object.defineProperty(myUser, 'firstName', {
        value: newFName
    })
}
myUser.setNewLastName = function (newLName) {
    Object.defineProperty(myUser, 'lastName', {
        value: newLName
    })
}



