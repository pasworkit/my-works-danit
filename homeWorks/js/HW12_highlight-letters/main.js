/*Чому для роботи з input не рекомендується використовувати клавіатуру?
В таких випадках не завжди є можливість "відловити" подію, наприклад пароль буде вставлений з буферу з допомогою правої клавіші миші і меню.
Деякі мобільні пристрої не генерують події клавіатури.
*/

document.addEventListener('keydown', (e) => {
    document.querySelectorAll('.btn').forEach(btn => {

        if (btn.dataset.item === e.code) {
            document.querySelector('.active')?.classList.remove('active');
            btn.classList.add('active');
        }
    });
});







