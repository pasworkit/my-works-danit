/*
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування?
Для використання спец. символів в js, як звичайних застосовують символ "слеш" - це і є екранування.
2. Які засоби оголошення функцій ви знаєте?
Існує 3 варіанти оголошення функції:
- функція оголошується з допомогою ключового слова - function

function getData(message) {
    let inputBirthday = prompt(message);
    return new Date(+inputBirthday.slice(6, 10), +inputBirthday.slice(3, 5) - 1, +inputBirthday.slice(0, 2));
}

- функція також оголошується з допомогую ключового слова - function, але не має назви і присвоюється змінній
const getData = function(message) {
    let inputBirthday = prompt(message);
    return new Date(+inputBirthday.slice(6, 10), +inputBirthday.slice(3, 5) - 1, +inputBirthday.slice(0, 2));
}
- функція також оголошується з допомогую ключового слова - function, має назву і присвоюється змінній, але визивається за допомогою імені змінної
const getData = function myBirthday(message) {
    let inputBirthday = prompt(message);
    return new Date(+inputBirthday.slice(6, 10), +inputBirthday.slice(3, 5) - 1, +inputBirthday.slice(0, 2));
}
3. Що таке hoisting, як він працює для змінних та функцій?
Механізм реалізований в js, коли змінні та функції можуть використовуватись, хоча вони написані будь-де в коді.
 */


const valueValidator = (value) => value === null || value === "";

function getValue(message, validator) {

    let inputValue = null;

    do {
        inputValue = prompt(message)
    } while (validator(inputValue))

    return inputValue;
}

function getData(message) {

    let inputBirthday = prompt(message);

    return new Date(+inputBirthday.slice(6, 10), +inputBirthday.slice(3, 5) - 1, +inputBirthday.slice(0, 2));
}

function createNewUser() {
    return {
        firstName: getValue('Enter first name', valueValidator),
        lastName: getValue('Enter last name', valueValidator),
        birthday: getData('Enter birthday in the format dd.mm.yyyy'),


        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },

        getAge() {
            let dateThisMoment = new Date;

            if (dateThisMoment.getMonth() > this.birthday.getMonth() || dateThisMoment.getMonth() === this.birthday.getMonth() && dateThisMoment.getDate() >= this.birthday.getDate()) {
                return dateThisMoment.getFullYear() - this.birthday.getFullYear();
            }
            return dateThisMoment.getFullYear() - this.birthday.getFullYear() - 1;
        },


        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        },
    }
}

const myUser = createNewUser();
console.log(myUser);
console.log(myUser.getAge());
console.log(myUser.getPassword());


Object.defineProperties(myUser, {
    firstName: {
        writable: false,
        configurable: true
    },
    lastName: {
        writable: false,
        configurable: true,
    }
});

myUser.setNewFirstName = function (newFName) {
    Object.defineProperty(myUser, 'firstName', {
        value: newFName
    })
}
myUser.setNewLastName = function (newLName) {
    Object.defineProperty(myUser, 'lastName', {
        value: newLName
    })
}




