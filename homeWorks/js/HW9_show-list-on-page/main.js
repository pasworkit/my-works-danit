/* 1. Опишіть, як можна створити новий HTML тег на сторінці.
document.createElement(tag)

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр функції це позиція, куди буде вставлено код відносно елемента у якого визвано функцію.
'beforebegin': до самого елементу;
'afterend': після елементу;
'afterbegin': в елемент, на його початку;
'beforeend': в елемент, в кінці;

3. Як можна видалити елемент зі сторінки?
Element.remove()
*/

const chekArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper", ["Pavlograd", "Novomoskovsk"]];

function createHTML(arr, parent = document.body) {
    const ulElem = document.createElement("ul");
    parent.append(ulElem);
    arr.map(elem => {
        if (Array.isArray(elem)) {
            return createHTML(elem, parent.querySelector('ul'))
        }
        return ulElem.insertAdjacentHTML('beforeend', `<li>${elem}</li>`);
    })

}

createHTML(chekArr);


