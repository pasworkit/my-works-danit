"use strict"
/*1. Які існують типи даних у Javascript?
 В JS є 8 типів данних:
 number - числовий, в діапазоні ±2**53, для цілих чисел та з плаваючою крапкою;
 string - рядки, від одного символу і більше;
 boolean - логічний тип, приймає true та false;
 null - для невідомих значень;
 undefined - для не присвоєнних значень;
 object - назва говорить сама за себе;
 symbol - унікальний ідентифікатор;
 bigint - для цілих чисел, більше 2**53-1;
2. У чому різниця між == і ===?
 Це оператори порівняння, при == проходить не явне приведення типів, при === , має назву "строгое равнство", окрім величин,
 проходить порівняння типів.
3. Що таке оператор?
  Оператор - це спец. символ або вираз, для операцій.
 */

let userName = prompt("Enter your name, please.");
while (userName === "" || userName === null) {
    userName = prompt("Enter your name, please.", [userName]);
}
let userAge = prompt("Enter your age, please.");
while (userAge === "" || userAge === null || isNaN(+userAge)) {
    userAge = prompt("Enter your age, please.", [userAge]);
}

if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
    let userAnswer = confirm("Are you sure you want to continue?");

    if (userAnswer) {
        alert("Welcome, " + userName);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome, " + userName);
}