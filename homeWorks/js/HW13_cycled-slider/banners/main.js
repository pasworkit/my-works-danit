/*
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
setTimeout() - дозволяє визвати задану функцію, через встановлений проміжок часу - один раз,
setInterval() - дозволяє визвати задану функцію, через заданий інтервал часу -постійно.

2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Нульова затримка - передбачає виконання функції, як можна швидше, після виконання кода.
Миттєвість буде залежати від черги, на скільки вона заповнена, точно спрогнозувати не можливо.

3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
setInterval() може працювати і в фоновому режимі а також залишається в пам'яті до виклику функції clearInterval(),
відповідно це вливає на пам'ять та ресурси.
*/

const imgCollection = document.querySelectorAll(".image-to-show");
const btnStop = document.querySelector(".btn-stop");
const btnContinue = document.querySelector(".btn-continue");

let i = 1;

const changePhoto = () => {
    document.querySelector(".active").classList.remove("active");
    imgCollection[i].classList.add("active");
    i++;
    if (i > imgCollection.length - 1) {
        i = 0;
    }
}

let timerId = setInterval(changePhoto, 3000);


btnStop.addEventListener("click", (e) => {
    clearInterval(timerId);
    e.target.disabled = true;
    btnContinue.disabled = false;
})

btnContinue.addEventListener("click", (e) => {
    timerId = setInterval(changePhoto, 3000)
    e.target.disabled = true;
    btnStop.disabled = false;
})




